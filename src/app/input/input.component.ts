import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  @Input() private type: string; // specifies type of the input - text, password, email etc
  @Input() private name: string; // specifies name of the input
  constructor() { }
  ngOnInit() {

  }

}
