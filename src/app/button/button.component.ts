import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  @Input() private btnName: string; // button text
  @Output() clickEvent = new EventEmitter<{ 'event': Event}>(); // emitter to parent
  constructor() { }

  ngOnInit() {
  }

  handleClick(event: Event) {
   console.log('clicked - inside component');
   this.clickEvent.emit({'event': event });
 }
}
